class Person {
  constructor() {
    if (typeof Person.instance === 'object') {
      return Person.instance;
    }

    Person.instance = this;

    return this;
  }
}

const john = new Person();
const john2 = new Person();

console.assert(john === john2, 'Should return true');

